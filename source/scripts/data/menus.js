export const menuinicio = [
  {
    title: 'Inicio',
    href: '/',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: '#nosotros',
    icon: 'icon-users'
  },
  {
    title: 'Contactos',
    href: '#contactos',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Baterias',
    href: 'baterias',
    submenu: [
      {
        title: 'Para automóviles',
        href: 'para-automoviles'
      },
      {
        title: 'Para motocicletas',
        href: 'para-motocicletas'
      }
    ]
  },
  {
    title: 'Accesorios',
    href: 'accesorios',
    submenu: [
      {
        title: 'Terminales',
        href: 'terminales'
      },
      {
        title: 'Cables',
        href: 'cables'
      },
      {
        title: 'Sujetadores',
        href: 'sujetadores'
      },
      {
        title: 'Densimetros',
        href: 'densimetros'
      }
    ]
  },
  {
    title: 'Cargadores',
    href: 'cargadores'
  },
  {
    title: 'Tésters digitales',
    href: 'testers-digitales'
  },
  {
    title: 'Agua desmineralizada',
    href: 'agua-desmineralizada'
  },
  {
    title: 'Reciclamos su bateria',
    href: 'reciclamos-su-bateria'
  }
]

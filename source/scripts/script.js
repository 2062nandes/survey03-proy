import { menuToggle } from './modules/menu'
import { getdate } from './modules/date'
import { edTabs } from './modules/tabs'
import { goups } from './modules/goup'
import { effectsEntry } from './modules/effectsEntry'
// import { videoSize } from "./modules/video";

// Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
// import { tns } from './libraries/tinyslider'
// import { optionSlider } from './modules/slider'

// import Data Vue
import { pathPage, pathMedia } from './data/routes'
import { menuinicio, mainmenu } from './data/menus'
import { contactform } from './data/contact-form'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
import { googleMap } from './components/googlemaps'
import Headroom from 'headroom.js/dist/headroom.min'

Vue.use(VueResource)

// Vue Components
Vue.component('google-map', googleMap)

const rex = new Vue({
  el: '#rex',
  data: {
    path_media: pathMedia,
    path_page: pathPage,
    menuinicio,
    mainmenu,
    formSubmitted: false,
    vue: contactform
  },
  mounted: function () {
    window.edTabs = edTabs
    // const slider = tns(optionSlider)
    goups()
    effectsEntry()
    menuToggle()
    getdate()
    const header = document.querySelector('.main-header')

    let headroom = new Headroom(header, {
      offset: 140,
      tolerance: {
        up: 5,
        down: 0
      },
      classes: {
        initial: 'header-initial', // Cuando el elemento se inicializa
        pinned: 'header-up', // Cuando se deplaza hacia arriba
        unpinned: 'header-down', // Cuando se deplaza hacia abajo
        top: 'header-top', // Cuando esta pegado arriba
        notTop: 'header-notop', // Cuando no esta pegado arriba
        bottom: 'header-bottom', // Cuando esta pegado abajo
        notBottom: 'header-nobottom'// Cuando no esta pegado abajo
      }
    })

    headroom.init()
  },
  methods: {
    isFormValid: function () {
      return this.nombre !== ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    }
  }
})
Vue.use(rex)

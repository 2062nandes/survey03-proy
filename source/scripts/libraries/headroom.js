/*
  |--------------------------------------------------------------------------
  | Headroom.js
  |--------------------------------------------------------------------------
  |
  | npm install headroom.js --save
  |
  */
import Headroom from 'headroom.js'

export let header = new Headroom(document.querySelector('.main-header'), {
  offset: 160,
  tolerance: {
    up: 5,
    down: 0
  },
  classes: {
    initial: 'header-initial', // Cuando el elemento se inicializa
    pinned: 'header-up', // Cuando se deplaza hacia arriba
    unpinned: 'header-down', // Cuando se deplaza hacia abajo
    top: 'header-top', // Cuando esta pegado arriba
    notTop: 'header-notop', // Cuando no esta pegado arriba
    bottom: 'header-bottom', // Cuando esta pegado abajo
    notBottom: 'header-nobottom'// Cuando no esta pegado abajo
  }
})
